# CORLI GUM (Corpus "à la GUM" i.e. GUM -- Georgetown University Multilayer Corpus -- like)

Le [consortium CORLI (Corpus, Langues, Interactions)](https://corli.huma-num.fr/), coordonné par Christophe Parisse
(CNRS, laboratoire Modyco) et Céline Poudat (Université Côte d'Azur, laboratoire BCL) et géré
par la Maison Européenne des Sciences de l'homme et de la société ( MESHS) de Lille, réunit
des chercheurs et enseignants-chercheurs en linguistique, et se donne pour objectif de fédérer
les équipes et laboratoires, les chercheurs, enseignants chercheurs, ou ingénieurs engagés
dans la production et le traitement de corpus numériques écrits et oraux, quels que soient la
langue et/ou le système d’écriture considérés.

Une des missions que s'est donné le consortium CORLI est de mettre à disposition de cette
communauté des ressources et des outils permettant de mieux travailler avec des corpus
langagiers.

Dans ce contexte, le consortium s’est donné plusieurs missions dont :
1. la mise à disposition d'une plateforme d'annotation collaborative
2. la constitution d’une ressource annotée à plusieurs niveaux par plusieurs annotateurs ayant a minima une licence en linguistique

Concernant la mise à disposition d’une plateforme d’annotation, il s’agit de proposer un outil accessible et utilisable par des utilisateurs non informaticiens et permettant une annotation linguistique de corpus diversifiés. 
Ces corpus diversifiés peuvent présenter une structuration particulière, des méta-données très spécifiques et des données non standards. 
Les annotations linguistiques envisagées peuvent être de bas comme de haut niveau. Elles relèvent nécessairement de différents niveaux : segmentation (en phrases, mots, unités minimales de discours, ...), étiquetage morpho-syntaxique, analyse syntaxique, détection des entités nommées, annotation sémantique et discursive (annotation des chaînes de référence, des relations de discours et d’objets linguistiques plus spécifiques comme le discours rapporté, les attaques, l’ironie, etc.).

Concernant la ressource, il s'agit d'enrichir chaque année une ressource en ajoutant de nouveaux textes et en complétant les différentes couches d'annotations afin d'aboutir à un corpus diversifié totalement annoté avec des annotations de qualité, réalisées par une diversité d'annotateurs ayant eu une formation en linguistique et pour lesquelles une étape d'adjudication a nécessairement été réalisée. 

La suite des explications est fournie dans le [Wiki de ce projet](https://gitlab.com/Consortium-CORLI/CORLI-MASC/-/wikis/CORLI-GUM-accueil), Wiki qui est régulièrement nourri et mis à jour par les participants au projet.
